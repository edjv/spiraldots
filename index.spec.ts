import {
    generateSpiral,
    drawGrid
} from './index';

describe("", function() {
    it("should generate spiral", function() {
        Array(200).fill(0)
            .map((_, i) => i+1)
            .map(generateSpiral)
            .forEach(drawGrid);
    });
});

enum Tile {
    Empty,
    Dot
}

type Row = Tile[];
type Grid = Row[];

class Position {
    x: number;
    y: number;
    constructor(x: number, y: number) { this.x = x; this.y = y; }
}

enum Direction {
    Down,
    Right,
    Up,
    Left
}

export function generateGrid(...dims: number[]): Grid {
    const longest = Math.max(...dims)
    const sideLength = longest % 2 == 0 ?
        longest + 1 : longest;
    return Array(sideLength).fill(0)
        .map(row => Array(sideLength).fill(0)
            .map(tile => Tile.Empty));
}

export function fillDots(grid: Grid, dots: Position[]): Grid {
    const center = new Position(
        (grid[0].length - 1)/2,
        (grid.length - 1)/2
    );
    return dots
        .map(({x, y}) => new Position(center.x + x, center.y + y))
        .reduce((grid, {x, y}) => {
            grid[y][x] = Tile.Dot;
            return grid;
        }, grid)
}

export function generateSpiral(numDots: number): Grid {
    let dots = [new Position(0, 0)];
    let [x, y] = [0, 0];
    let [maxX, maxY] = [x, y];
    let numRemainingDots = numDots - 1;

    let currentDirection = Direction.Down;
    let stepsBeforeTurn = 1;
    let stepsToGo = 1;

    while (numRemainingDots > 0) {
        if (stepsToGo == 0) {
            if (currentDirection == Direction.Down)
                currentDirection = Direction.Right;
            else if (currentDirection == Direction.Right)
                currentDirection = Direction.Up;
            else if (currentDirection == Direction.Up)
                currentDirection = Direction.Left;
            else if (currentDirection == Direction.Left)
                currentDirection = Direction.Down;

            stepsBeforeTurn += 1;
            stepsToGo = stepsBeforeTurn;
        }

        if (currentDirection == Direction.Down) {
            y += 1;
            if (y > maxY)
                maxY = y;
        }
        else if (currentDirection == Direction.Right) {
            x += 1;
            if (x > maxX)
                maxX = x;
        }
        else if (currentDirection == Direction.Up) {
            y -= 1;
            if (y < -maxY)
                maxY = -y;
        }
        else if (currentDirection == Direction.Left) {
            x -= 1;
            if (x < -maxX)
                maxX = -x;
        }

        dots.push(new Position(x, y));
        numRemainingDots -= 1;
        stepsToGo -= 1;
    }

    let grid = generateGrid(maxX*2, maxY*2);
    grid = fillDots(grid, dots);
    return grid;
}

export function drawGrid(grid: Grid): void {
    console.log("--BEGIN--")
    grid
        .map(row =>
            row.map(tile => tile == Tile.Empty ? " " : "*")
                .join(" ")
        )
        .forEach(row => console.log(row))
    console.log("---END---")
}
